/**
 * @format
 * @flow
 **/
import React, { useState } from "react";
import { HeaderSection } from "./sub-components/HeaderSection";
import { DataTable } from "./sub-components/DataTable";
import { getUserDataFromJson } from "../utils/index";

const leaderboardData = require("../../assets/leaderboard.json");

const CaseStudyLeaderboard = () => {
  const [searchValue, setSearchValue] = useState(null);
  const [userData, setUserData] = useState(leaderboardData);
  const onSubmit = () => {
    console.log("userData xxx", userData);
  };
  const data = getUserDataFromJson(userData, searchValue);
  console.log("searchValue", searchValue);
  return (
    <>
      <HeaderSection setSearchValue={setSearchValue} />
      <DataTable data={data} />
    </>
  );
};

export { CaseStudyLeaderboard };

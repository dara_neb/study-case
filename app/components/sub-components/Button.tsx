/**
 * @format
 * @flow
 **/
import React from "react";
import { Text, TouchableOpacity } from "react-native";
import { styles } from "./styles/ButtonStyle";

const Button = ({ name, onPress }) => {
  const { containerStyle, buttonTextStyle } = styles;
  return (
    <TouchableOpacity style={containerStyle} onPress={onPress}>
      <Text style={buttonTextStyle}>{name}</Text>
    </TouchableOpacity>
  );
};

export { Button };

/**
 * @format
 * @flow
 **/
import React, { useState } from "react";
import { View } from "react-native";
import { Button } from "./Button";
import { SearchBox } from "./SearchBox";
import { styles } from "./styles/HeaderSectionStyle";

const HeaderSection = ({ setSearchValue }) => {
  const [searchValueTemp, setSearchValueTemp] = useState(null);
  const updateSearchValue = () => setSearchValue(searchValueTemp);
  const { containerStyle } = styles;
  return (
    <View style={containerStyle}>
      <SearchBox
        searchValue={searchValueTemp}
        setSearchValue={setSearchValueTemp}
      />
      <Button name={"Search"} onPress={updateSearchValue} />
    </View>
  );
};

export { HeaderSection };

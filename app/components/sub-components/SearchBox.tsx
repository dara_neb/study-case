/**
 * @format
 * @flow
 **/
import React from "react";
import { View, TextInput } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { APP_ICONS } from "../../constants/Commons";
import { styles } from "./styles/SearchBoxStyle";
import { APP_COLORS } from "../../constants/Commons";

const SearchBox = ({ searchValue, setSearchValue }) => {
  const { iconStyle, containerStyle } = styles;
  return (
    <View style={containerStyle}>
      <Icon
        name={APP_ICONS.SEARCH_ICON_NAME}
        color={APP_COLORS.BLACK_20}
        size={16}
        style={iconStyle}
      />
      <TextInput
        placeholder={"Username"}
        value={searchValue}
        onChangeText={setSearchValue}
      />
    </View>
  );
};

export { SearchBox };

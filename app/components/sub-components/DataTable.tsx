/**
 * @format
 * @flow
 **/
import React from "react";
import { View } from "react-native";
import _ from "lodash";
import { Table, Row, Rows } from "react-native-table-component";
import { getTableHeader } from "../../utils";
import { styles } from "./styles/DataTableStyle";

const DataTable = ({ data = [] }) => {
  const { containerStyle, textStyle, borderStyle, headerStyle } = styles;
  const tableHeadData = getTableHeader();
  return (
    <View style={containerStyle}>
      <Table borderStyle={borderStyle}>
        <Row data={tableHeadData} style={headerStyle} textStyle={textStyle} />
        <Rows data={data} textStyle={textStyle} />
      </Table>
    </View>
  );
};

export { DataTable };

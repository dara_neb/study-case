/**
 * @format
 * @flow
 **/

import { StyleSheet } from "react-native";
import { APP_COLORS } from "../../../constants/Commons";

export const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 15,
  },
});

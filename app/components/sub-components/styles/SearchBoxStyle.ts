/**
 * @format
 * @flow
 **/

import { StyleSheet } from "react-native";
import { APP_COLORS } from "../../../constants/Commons";

export const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    flexDirection: "row",
    height: 45,
    borderRadius: 10,
    backgroundColor: APP_COLORS.WHITE,
    shadowColor: APP_COLORS.BLACK_20,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 10,
    shadowOpacity: 1,
    marginHorizontal: 15,
  },
  iconStyle: {
    alignSelf: "center",
    paddingHorizontal: 10,
  },
});

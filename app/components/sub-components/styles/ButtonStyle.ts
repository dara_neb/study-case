/**
 * @format
 * @flow
 **/

import { StyleSheet } from "react-native";
import { APP_COLORS } from "../../../constants/Commons";

export const styles = StyleSheet.create({
  containerStyle: {
    alignSelf: "center",
    backgroundColor: APP_COLORS.WHITE,
    marginRight: 15,
  },
  buttonTextStyle: {
    fontSize: 16,
    fontWeight: "normal",
    fontStyle: "normal",
    color: APP_COLORS.BLACK_20,
  },
});

/**
 * @format
 * @flow
 **/

import { StyleSheet } from "react-native";
import { APP_COLORS } from "../../../constants/Commons";

export const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    padding: 16,
    backgroundColor: APP_COLORS.WHITE,
  },
  headerStyle: { height: 40, backgroundColor: APP_COLORS.SOFT_BLUE },
  textStyle: { margin: 6 },
  borderStyle: { borderWidth: 2, borderColor: APP_COLORS.BLACK_20 },
});

import _ from "lodash";
import { Alert } from "react-native";

const errorMessage =
  "This user name does not exist! Please specify an existing user name!";

export const getUserDataFromJson = (leaderboardData = {}, search = "") => {
  console.log("search", search);
  const userData = _.orderBy(
    _.toArray(leaderboardData),
    ["bananas", "stars"],
    ["desc", "desc"]
  );
  const factoryData = getFilteredUserData(search, userData);
  const constructUserData = _.map(factoryData, (item, index) => {
    const isSearchedUser = _.isEqual(search, item.name);
    const { name = "Unnamed", rank = index + 1, bananas } = item;
    // NOTE: Boolean value is not renderable within table, string accepted
    return [name, rank, bananas, `${isSearchedUser}`];
  });
  return constructUserData;
};

export const getFilteredUserData = (search = "", userData = []) => {
  const takenUserData = _.take(userData, 10);
  if (_.isEmpty(search)) {
    return takenUserData;
  } else {
    const allFilteredUserData = _.filter(userData, { name: search });
    const isSearchedUserNotFound =
      _.isEmpty(allFilteredUserData) && !_.isEmpty(search);
    if (isSearchedUserNotFound) {
      Alert.alert(errorMessage);
      return;
    } else {
      const filteredUserData = _.filter(takenUserData, { name: search });
      const isSearchedUserInTop10 =
        !_.isEmpty(filteredUserData) && !_.isEmpty(search);
      if (isSearchedUserInTop10) {
        return takenUserData;
      } else {
        const element = _.head(allFilteredUserData);
        takenUserData[9] = element;
        return takenUserData;
      }
    }
  }
};

export const getTableHeader = () => {
  return ["Name", "Rank", "Bananas", "isSearchedUser"];
};

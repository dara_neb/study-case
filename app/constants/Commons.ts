/**
 * @format
 * @flow
 **/
export const APP_COLORS = {
  BLACK_20: "rgba(0, 0, 0, 0.2)",
  WHITE: "#ffffff",
  SOFT_BLUE: "#f1f8ff",
};

export const APP_ICONS = {
  SEARCH_ICON_NAME: "search",
};

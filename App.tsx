/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from "react";
import type { Node } from "react";
import { SafeAreaView, ScrollView } from "react-native";
import { CaseStudyLeaderboard } from "./app/components/CaseStudyLeaderboard";

const App: () => Node = () => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <CaseStudyLeaderboard />
      </ScrollView>
    </SafeAreaView>
  );
};

export default App;
